<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach(range(1,10) as $index){
            DB::table('users')->insert([
               'name' => $faker->name,
               'email' => $faker->email,
               'password' => bcrypt('secret')
            ]);
        }

        foreach(range(1,100) as $index){
            DB::table('news')->insert([
                'title' => $faker->sentence,
                'subtitle' => $faker->sentence,
                'content' => $faker->realText(400),
                'user_id' => $faker->numberBetween(1,10)
            ]);
        }
    }
}
