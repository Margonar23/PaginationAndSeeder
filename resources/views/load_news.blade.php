{{--test pagination--}}
@foreach($news->chunk(3) as $chunkedNews)
    <div class="row">
        @foreach ($chunkedNews as $n)
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ $n->title }}</h3>
                    </div>
                    <div class="panel-body">
                        <h4>{{ $n->subtitle }}</h4>
                        <br>
                        <p>{{  str_limit($n->content, $limit = 150, $end = '...') }}</p>
                    </div>
                    <div class="panel-footer"><i class="font-italic">Author: {{ $n->user->name }}</i></div>
                </div>
            </div>
        @endforeach
    </div>
@endforeach

<div class="pull-right">
    {{ $news->links() }}
</div>